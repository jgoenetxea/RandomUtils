// Copyright 2019 <Copyright Owner>
// Server side C/C++ program to demonstrate Socket programming
#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>

#include <iostream>
#include <fstream>

#include "socketStream.h"

int main() {
    SocketStream ss("", 8080, SocketStream::SS_SERVER);

    bool running = true;
    while (running) {
        std::cout << "Waiting a client" << std::endl;
        if (ss.waitClient()) {
            std::cout << "Client reached" << std::endl;
            bool isReceiving = true;
            std::ofstream os;
            std::string buffer;
            while (isReceiving) {
                ss >> buffer;
                std::cout << "Recived message: " << buffer << std::endl;
                if (buffer[0] == '#') {
                    // Process command
                    char command = buffer[1];
                    switch (command) {
                    case 'o': {
                        // Open file
                        size_t size = buffer.size();
                        std::string filename = buffer.substr(3, size - 3);
                        std::cout << "Defined out file: " << filename
                                  << std::endl;
                        os.open(filename);
                        if (!os.is_open()) {
                            std::cout << "Error: Out file not open!"
                                      << std::endl;
                        }
                        break;
                    }
                    case 'c': {
                        // Close file
                        std::cout << "Close file" << std::endl;
                        os.close();
                        break;
                    }
                    case 'd': {
                        // Disconnect
                        std::cout << "Disconnecting from client" << std::endl;
                        isReceiving = false;
                        break;
                    }
                    case 'x': {
                        // Disconnect
                        std::cout << "Close server" << std::endl;
                        running = false;
                        break;
                    }
                    }
                } else {
                    // Write data in file
                    std::cout << "Write data in disk: " << buffer << std::endl;
                    os << buffer << std::endl;
                }

                // Send ack
                ss << "0";
            }
        } else {
            std::cout << "Error attaching client!" << std::endl;
        }

        printf("Hello message sent\n");
    }
    return 0;
}
