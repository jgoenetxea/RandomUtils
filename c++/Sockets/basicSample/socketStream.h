/* Copyright 2019 Vicomtech */
#ifndef SAMPLES_SAMPLE_REMOTESTORAGESERVER_SOCKETSTREAM_H_
#define SAMPLES_SAMPLE_REMOTESTORAGESERVER_SOCKETSTREAM_H_

#include <arpa/inet.h>

#include <string>

class SocketStream {
 public:
    enum SocketType {
        SS_SERVER,
        SS_CLIENT,
        SS_NONE
    };

    SocketStream();
    SocketStream(const std::string &_ip, const uint16_t _port,
                 const SocketType _type = SS_CLIENT);
    ~SocketStream();

    bool open(const std::string &_ip, const uint16_t _port,
              const SocketType _type);

    bool is_open();

    SocketType getType();

    bool waitClient();

    SocketStream& operator<<(const std::string &_msg);
    SocketStream& operator>>(std::string &_msg);

 private:
    bool m_isOpen = false;
    SocketType m_type = SS_NONE;
    int m_sock = 0;
    struct sockaddr_in m_serv_addr;

    // Client data
    int m_sockCli = 0;

    bool initClient(const std::string &_ip, const uint16_t _port);
    bool initServer(const uint16_t _port);

    bool sendMessage(const int sock, const std::string& _msg) const;
    bool reciveMessage(const int sock, std::string* _msg) const;

    // Invalidate copy constructor and assignation operator
 private:
    SocketStream(const SocketStream &) {}
    SocketStream& operator=(const SocketStream &) {}
};


#endif  // SAMPLES_SAMPLE_REMOTESTORAGESERVER_SOCKETSTREAM_H_
