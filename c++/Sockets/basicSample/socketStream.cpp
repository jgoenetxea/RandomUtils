/* Copyright 2019 Vicomtech */
#include "socketStream.h"

#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>

#include <stdexcept>

SocketStream::SocketStream() {
    m_isOpen = false;
}

SocketStream::SocketStream(const std::string &_ip, const uint16_t _port,
                           const SocketType _type) {
    if (!open(_ip, _port, _type)) {
        throw std::runtime_error("Error opening connection!");
    }

    m_isOpen = true;
}

SocketStream::~SocketStream() {
    if (m_isOpen) {
        close(m_sock);
        m_isOpen = false;
    }
}

bool SocketStream::open(const std::string &_ip, const uint16_t _port,
                        const SocketType _type) {
    if (_type == SS_NONE) return false;
    if (_type == SS_CLIENT) {
        m_type = SS_CLIENT;
        return initClient(_ip, _port);
    } else {
        m_type = SS_SERVER;
        return initServer(_port);
    }
}

bool SocketStream::is_open() {
    return m_isOpen;
}

SocketStream::SocketType SocketStream::getType() {
    return m_type;
}

bool SocketStream::waitClient() {
    int addrlen = sizeof(m_serv_addr);
    if ((m_sockCli = accept(m_sock, (struct sockaddr *)&m_serv_addr,
                            reinterpret_cast<socklen_t*>(&addrlen))) < 0) {
        perror("accept");
        return false;
    }
    return true;
}

SocketStream& SocketStream::operator<<(const std::string &_msg) {
    int sock = (m_type == SS_SERVER)?m_sockCli:m_sock;
    if (!sendMessage(sock, _msg)) {
        throw std::runtime_error("Error sending the message.");
    }
    return *this;
}

SocketStream& SocketStream::operator>>(std::string &_msg) {
    int sock = (m_type == SS_SERVER)?m_sockCli:m_sock;
    if (!reciveMessage(sock, &_msg)) {
        throw std::runtime_error("Error reciving the message.");
    }
    return *this;
}

bool SocketStream::initClient(const std::string &_ip, const uint16_t _port) {
    if ((m_sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return false;
    }
    memset(&m_serv_addr, '0', sizeof(m_serv_addr));

    m_serv_addr.sin_family = AF_INET;
    m_serv_addr.sin_port = htons(_port);

    // Convert IPv4 and IPv6 addresses from text to binary form
    if (inet_pton(AF_INET, _ip.c_str(), &m_serv_addr.sin_addr) <= 0)  {
        printf("\nInvalid address/ Address not supported \n");
        return false;
    }

    if (connect(m_sock, (struct sockaddr *)&m_serv_addr,
                sizeof(m_serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return false;
    }

    return true;
}

bool SocketStream::initServer(const uint16_t _port) {
    // Creating socket file descriptor
    if ((m_sock = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        return false;
    }

    int opt = 1;
    // Forcefully attaching socket to the port 8080
    if (setsockopt(m_sock, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT,
                                                &opt, sizeof(opt))) {
        perror("setsockopt");
        return false;
    }
    m_serv_addr.sin_family = AF_INET;
    m_serv_addr.sin_addr.s_addr = INADDR_ANY;
    m_serv_addr.sin_port = htons(_port);

    // Forcefully attaching socket to the port 8080
    if (bind(m_sock, (struct sockaddr *)&m_serv_addr,
             sizeof(m_serv_addr)) < 0) {
        perror("bind failed");
        return false;
    }
    if (listen(m_sock, 3) < 0) {
        perror("listen");
        return false;
    }

    return true;
}

bool SocketStream::sendMessage(const int sock, const std::string& _msg) const {
    ssize_t s = send(sock, _msg.c_str(), _msg.size(), 0);
    if (s < 0) {
        return false;
    }
    return true;
}

bool SocketStream::reciveMessage(const int sock, std::string* _msg) const {
    char buffer[1024] = {0};
    ssize_t s = read(sock, buffer, 1024);
    if (s < 0) {
        return false;
    }
    std::string tmpStr(buffer);
    *_msg = tmpStr.substr(0, static_cast<size_t>(s));
    return true;
}
