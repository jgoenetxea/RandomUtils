// Copyright 2019 <Copyright Owner>
// Client side C/C++ program to demonstrate Socket programming
#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>

#include <iostream>
#include <string>
#include <vector>
#include <exception>

#include "socketStream.h"

#define PORT 8080

void waitACK(SocketStream *_ss) {
    std::string buffer;

    while (true) {
        try {
            *_ss >> buffer;
        } catch (std::exception e) {
            std::cout << e.what() << std::endl;
        }

        if (buffer[0] == '0') {
            return;
        }
    }
}

int main() {
    std::cout << "Start" << std::endl;
    std::string ip = "127.0.0.1";
    uint16_t port = 8080;

    SocketStream ss(ip, port);

    std::string openFile = "#o:test_file.txt\0";
    std::string closeFile = "#c\0";
    std::vector<std::string> values = {"0 1 2\0", "4 5 6\0", "7 8 9\0"};
    std::string closeConnection = "#d\0";

    ss << openFile;
    waitACK(&ss);
    for (const auto& v : values) {
        ss << v;
        waitACK(&ss);
    }
    ss << closeFile;
    waitACK(&ss);
    ss << closeConnection;
    std::cout << "Finished" << std::endl;
    return 0;
}
