#!/bin/bash

#set -x #echo on
# this script is for profiling a linux app:
# Remember to include the debug symbols in the compilation.

# ------------------------------- #
#  Modificable variable block     #
# ------------------------------- #
# +++++++++++++++++++++++++++++++ #
PROJECT_DIR="path to the binary file"
PROJECT_EXE="binary_file_name"
DURATION="30"
OUTPUT_PERF_FILE="perf.data"

# +++++++++++++++++++++++++++++++ #
# -------------------------------------- #
#  Do not modify code above this message #
# -------------------------------------- #
RECORDING_COMMAND="/usr/bin/perf record"
RECORDING_OPTIONS="-e cpu-clock,faults --call-graph fp sleep $DURATION"

REPORTING_COMMAND="/usr/bin/perf report"
REPORTING_OPTIONS="--sort comm,dso,symbol -g"

clear				# clear terminal window
#it should put the libraries
echo "Perf profiler script."
echo "Trying to profile $PROJECT_EXE"
echo

echo 'Please enter your choice:'
PS3='> '
options=("Record data" "Show report" "Show call graph" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Record data")
            # Run the application
            clear
            # echo -n "Running the app..."
            # $PROJECT_DIR/$PROJECT_EXE &
            # sleep 3
            echo -n "Please RUN your $PROJECT_EXE app and press return when is wroking >"
            read text

            # Get the pid of the process
            COMMAND_RESULT=$(ps -ef | grep $PROJECT_EXE)
            echo "Looking for the PID"
            TPID="$(echo "$COMMAND_RESULT"| awk '{print $2}')"
            set -- $TPID
            PID=$1
            echo "Proccess ID = $PID"
            ERROR=$(cat /proc/$PID/maps | grep boot.oat)
            echo "$ERROR"

            echo "Recording data"
            KPTR_RESTRICT=$(cat /proc/sys/kernel/kptr_restrict)
            if [ "$KPTR_RESTRICT" = "1" ]
            then
              $(sudo sh -c " echo 0 > /proc/sys/kernel/kptr_restrict")
            fi
            echo "Recording the app with command:"
            echo "> $RECORDING_COMMAND -p $PID -o $OUTPUT_PERF_FILE $RECORDING_OPTIONS"
            RECORDING_RESULT=$($RECORDING_COMMAND -p $PID -o $OUTPUT_PERF_FILE $RECORDING_OPTIONS)
            echo "Recording result $RECORDING_RESULT"
            ;;
        "Show report")
            echo "Select report file [$OUTPUT_PERF_FILE]: "
            read USERFILE
            if [ "$USERFILE" = "" ]
            then
              USERFILE=$OUTPUT_PERF_FILE
            fi
            echo "Show the report '$USERFILE'"
            $REPORTING_COMMAND $REPORTING_OPTIONS -i $USERFILE
            ;;
        "Show call graph")
            echo "Select report file [$OUTPUT_PERF_FILE]: "
            read USERFILE
            if [ "$USERFILE" = "" ]
            then
              USERFILE=$OUTPUT_PERF_FILE
            fi
            echo "Show the call-graph report for '$USERFILE'"
            $REPORTING_COMMAND --sort=cpu -i $USERFILE
            ;;
        "Quit")
            break
            ;;
        *) echo invalid option;;
    esac
    echo "Please enter your choice:"
done

echo "Bye!"
