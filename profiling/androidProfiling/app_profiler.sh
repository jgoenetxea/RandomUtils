#!/bin/bash
#set -x #echo on
# this script is for profiling the android app:
#please execute your app in debug mode and with the symbols.
#define your variables:

# ------------------------------- #
#  Modificable variable block     #
# ------------------------------- #
# +++++++++++++++++++++++++++++++ #
PROJECT_DIR="dir to he project code to be analyzed"
APP_NAME="app-apk-name.apk"
DEVICE_TMP_DIR="/data/local/tmp"
DURATION="30"
SIMPLEPERF_DIR="full path to the simpleperf executable"
OUTPUT_PERF_FILE="full path where the output data should be stored (in the sd-card)"
APK_PACKAGE="package name"

REINSTALL_APP="y"
ENABLE_RECORD="y"
ENABLE_REPORTING="y"
SHOW_CALL_GRAPH="y"

# +++++++++++++++++++++++++++++++ #
# -------------------------------------- #
#  Do not modify code above this message #
# -------------------------------------- #

APK_PATH="$PROJECT_DIR/app/build/outputs/apk"
RUNAS="run-as"
ADB_SHELL="adb shell"

RUNAS_FULL_COMMAND="$ADB_SHELL $RUNAS $APK_PACKAGE"
RECORDING_COMMAND="$RUNAS_FULL_COMMAND ./simpleperf record"
REPORTING_COMMAND="$RUNAS_FULL_COMMAND ./simpleperf report"

SIMPLEPERF_EXEC_LOCATION="$SIMPLEPERF_DIR/simpleperf/scripts/bin/android"

RECORDING_OPTIONS="--duration $DURATION --dump-symbols --call-graph fp"
#--call-graph fp
REPORTING_OPTIONS="--sort comm,pid,tid,symbol"

PERF_OUTPUT="-o $OUTPUT_PERF_FILE"

SIMPLEPERF_PYTHON_SCRIPTS_DIR="$SIMPLEPERF_DIR/simpleperf/scripts"

clear				# clear terminal window
LIB_ARCH="arm64"
REPORTING_LIBRARIES_PATH="/data/app/$APK_PACKAGE-2/lib/$LIB_ARCH"

#it should put the libraries
echo "Profiler script."
echo "trying to install $APK_PACKAGE/$APP_NAME"

echo  "please unnistall your apk if there is installed."
echo
#cd $PROJECT_DIR

#gradlew clean assemble
if [ "$REINSTALL_APP" = "y" ]
then
adb install -r $APK_PATH/$APP_NAME
fi
echo "setting up setprop security.perf_harden 0 to kernel"
echo
$ADB_SHELL setprop security.perf_harden 0
adb root
adb shell echo 0 > /proc/sys/kernel/kptr_restricts


  echo -n "What is your android distribution? (L/M/N)?"
  read option
  echo "your option $option"
if [ "$option" = "N" ]
then
  echo "setting to android Nougat 7.0"
  echo

  echo "Generating debug info: setprop debug.generate-debug-info true"
  echo
  $ADB_SHELL setprop debug.generate-debug-info true
  echo "COMPILING $APK_PACKAGE"
  echo
  $ADB_SHELL cmd package compile -f -m speed $APK_PACKAGE
  echo "forcing-stop to changes make effect"
  $ADB_SHELL am force-stop $APK_PACKAGE
  echo "copying simpleperf with the correct architecture:"
  adb push $SIMPLEPERF_EXEC_LOCATION/arm64/simpleperf $DEVICE_TMP_DIR
fi
if [ "$option" = "M" ]
then
  echo "setting to android Marshallow 6.0"
  echo "enabling debug flags dalvik.vm.dex2oat-flags -g"
  adb root
  $ADB_SHELL setprop dalvik.vm.dex2oat-flags -g
  echo "copying simpleperf with the correct architecture:"
  adb push $SIMPLEPERF_EXEC_LOCATION/arm64/simpleperf $DEVICE_TMP_DIR
fi
if [ "$option" = "L" ]
then
  echo "setting to android Lollipop 5.0"
  echo "enabling debug flags dalvik.vm.dex2oat-flags --include-debug-symbols"
  adb root
  $ADB_SHELL setprop dalvik.vm.dex2oat-flags --include-debug-symbols
  echo "copying simpleperf with the correct architecture:"
    adb push $SIMPLEPERF_EXEC_LOCATION/arm/simpleperf $DEVICE_TMP_DIR
fi
clear
echo -n "please RUN your $APK_PACKAGE app and press return when is wroking >"
read text
COMMAND_RESULT=$($RUNAS_FULL_COMMAND ps | grep androidTest)
PID="$(echo "$COMMAND_RESULT"| awk '{print $2}')"
echo "proccess_PID $PID"
echo
ERROR=$($RUNAS_FULL_COMMAND cat /proc/$PID/maps | grep boot.oat)
echo "$ERROR"

$RUNAS_FULL_COMMAND cp $DEVICE_TMP_DIR/simpleperf .

  set -x
#$RUNAS_FULL_COMMAND echo 0 > /proc/sys/kernel/kptr_restrict
if [ "$ENABLE_RECORD" = "y" ]
then
  echo "recording the app:"
  RECORDING_RESULT=$($RECORDING_COMMAND -p $PID -o $OUTPUT_PERF_FILE $RECORDING_OPTIONS)
  echo "recording result $RECORDING_RESULT"
fi
#adb root
#$RUNAS_FULL_COMMAND cp perf.data /data/local/tmp/perf.data

if [ "$ENABLE_REPORTING" = "y" ]
then
  echo "reporting the app"
  #REPORT_RESULT=$($RUNAS_FULL_COMMAND ./simpleperf report --sort pid)

  libname="YYYYY" # native library name
  $REPORTING_COMMAND --dsos $REPORTING_LIBRARIES_PATH/lib$libname.so -i $OUTPUT_PERF_FILE $REPORTING_OPTIONS
  libname="XXXX"  # native library name
  $REPORTING_COMMAND --dsos $REPORTING_LIBRARIES_PATH/lib$libname.so -i $OUTPUT_PERF_FILE $REPORTING_OPTIONS

  echo "reporting result $report_result"

fi
if [ "$SHOW_CALL_GRAPH" = "y" ]
then
 adb pull $OUTPUT_PERF_FILE ./perf.data
 MYDIR=$(pwd)
 echo $MYDIR
 python $SIMPLEPERF_PYTHON_SCRIPTS_DIR/report.py -g
fi
