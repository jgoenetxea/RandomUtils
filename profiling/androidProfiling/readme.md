# app_profiler.sh
---
This scritps tries to automatize the android profiling to see where are the bottlenecks in the code:
## USAGE:
- sh app_profiler.sh

## HOW TO PUT THE PROFILER WORKING:
First, you need to have the android application build correctly configured for the profiling.
Once the application is properly compiled, you have to follow some steps to put the profiling script working.

### Select 'profiling' as build_variant
* In Android Studio, go to build -> select build variant.
* The build variant menu should appear in the bottom left corner of the Android Studio window.
* Then select 'profiling' in the drop-down menu next to 'app'.

### Compile and build the .APK using Android Studio.
* In Android Studio, go to build -> build-apk.
* The '.apk' package should be generated in the path:
> [repo path]/androidTest/app/build/outputs/apk
* This path is needed for the script configuration step.

### Get Simpleperf code:
* Download simpleperf from:
> $ git clone https://android.googlesource.com/platform/system/extras

### Configure the script variables
* You need to define some variables in the script to point the new app.
    * __APP_NAME__: the .APK file name.
    * __PROJECT_DIR__: the project location in the pc where the root build.gradle is located.
    * __SIMPLEPERF_LOCATION__: the path to the 'extras' folder you cloned in the previous step.
    * __DEVICE_TMP_DIR__: the temporal folder of your smartphone //usually /data/local/tmp
    * __DURATION__: recording duration in secs.
    * __OUTPUT_PERF_FILE__: the output perf.data file to evaluate later.

### Define the execution constraints
* __REINSTALL_APP__="y" -> reinstall the app
* __ENABLE_RECORD__="y" -> activate recording.
* __ENABLE_REPORTING__="y" -> enable reporting.
* __SHOW_CALL_GRAPH__="y" -> show call graph.

### Run the profiler
* Connect the device to be used to the computer.
* Open a terminal and go to the path where the 'app_profiler.sh' scritp is located. Something like:
> $ cd [repo path]/tests/androidProfiling

* If a permission error appear in the console, go to the terminal and set it up as PTP (_Photo transfer_) mode.
* Wait until the app is intalled (can be take some seconds) and then follow the instructions in the console.
* The console should show the next message:
> What is your android distribution? (L/M/N)?

  You have to press 'L' if the Android version installed in the device is Lollipop (5.0-5.1.1), 'M' if it is Marshmallow (6.0-6.0.1) or 'N' if it is Nougat (7.0-7.1.2)

### The feedback
If the option 'SHOW_CALL_GRAPH' was set to _y_, after the data recording, a feedback window should appear with the call stack tree of the recorded execution.
You can navigate in this tree to check and detect possible bottlenecks.


## Tutorials:
You can find further information in:
https://android.googlesource.com/platform/system/extras/+/master/simpleperf/README.md#prepare-an-android-application

## Important:
* Tested in android 6.0
* Not every device is able to profile (it depends on the hadware and what android version image is using).
* Until device android version is >= android 7.0 the emulators are not capable to profile.
